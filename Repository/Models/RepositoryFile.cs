﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
	public class RepositoryFile
	{
		[Key]
		[StringLength(150)]
		public string Id { get; set; }

		public ApplicationUser Owner { get; set; }

		[Required]
		[StringLength(200)]
		public string OwnerId { get; set; }

		[Required]
		[StringLength(200)]
		public string Name { get; set; }

		[StringLength(1000)]
		public string Description { get; set; }

		[Required]
		[StringLength(500)]
		public string FilePath { get; set; }		
	}
}
