﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Repository.ViewModels
{
	public class FileFormViewModel
	{
		[StringLength(1000)]
		public string Description { get; set; }

		[Required]
		public IFormFile File { get; set; }
	}
}
