﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Repository.Data;
using Repository.Models;
using Repository.ViewModels;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Repository.Controllers
{
	public class FileController : Controller
	{
		private ApplicationDbContext _context;
		private UserManager<ApplicationUser> _manager;
		private IHostingEnvironment _environment;


		public FileController(IConfiguration configuration, UserManager<ApplicationUser> manager, IHostingEnvironment environment)
		{
			var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
			optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
			_context = new ApplicationDbContext(optionsBuilder.Options);

			_manager = manager;

			_environment = environment;
		}

		[HttpGet]
		[Authorize]
		public IActionResult Create()
		{
			return View();
		}

		[Authorize]
		[HttpPost]		
		[RequestSizeLimit(209715200)]
		public async Task<IActionResult> Create(FileFormViewModel viewModel, [FromForm] IFormFileCollection uFile)
		{
			if (!ModelState.IsValid)
			{
				return View("Create", viewModel);
			}

			var user = await _manager.GetUserAsync(HttpContext.User);

			string email = user.Email;

			string pathToFile = Path.Combine(_environment.WebRootPath, "Files", email);			

			if (!Directory.Exists(pathToFile))
			{
				Directory.CreateDirectory(pathToFile);
			}

			pathToFile = Path.Combine(pathToFile, viewModel.File.FileName);

			using (var fileStream = new FileStream(pathToFile, FileMode.Create))
			{
				await viewModel.File.CopyToAsync(fileStream);
			}

			var file = new RepositoryFile
			{
				OwnerId = user.Id,
				Name = viewModel.File.FileName,
				Description = viewModel.Description,
				FilePath = pathToFile,
			};

			_context.Files.Add(file);
			_context.SaveChanges();

			return RedirectToAction("Index", "Home");
		}

		[Authorize]		
		public async Task<IActionResult> ManageFiles()
		{
			var user = await _manager.GetUserAsync(HttpContext.User);

			IEnumerable<RepositoryFile> files = _context.Files.ToList().Where(f => f.OwnerId == user.Id);



			return View("ManageFiles", files);
		}

		[Authorize]
		public FileResult Download(string fileId)
		{
			var file = _context.Files.First(f => f.Id == fileId);
			var filepath = file.FilePath;
			byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);
			return File(fileBytes, "application/binary", file.Name);
		}


	}	
}